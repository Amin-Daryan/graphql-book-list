import React, { useEffect, useState } from 'react';
import { useLazyQuery } from '@apollo/client';
import { getBookQuery } from "../queries/queries"


function BookDetails({ selectedBook }) {
    const [getBookDetails, { data }] = useLazyQuery(getBookQuery)
    const book = data && data.book

    useEffect(() => {
        if (selectedBook) getBookDetails({ variables: { id: selectedBook } })
    }, [selectedBook])

    return (
        <div id="book-details">
            {
                book &&
                <div>
                    <h2>{book.name}</h2>
                    <p>{book.genre}</p>
                    <p>{book.author && book.author.name}</p>
                    <p>Books by this author: </p>
                    <ul className="other-books">
                        {book.author && book.author.books.map(theBook => <li key={theBook.id}>{theBook.name}</li>)}
                    </ul>
                </div>
            }
        </div>
    )
}

export default BookDetails;