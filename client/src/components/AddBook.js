import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from "../queries/queries"


function AddBooks() {
    const { loading, data, refetch } = useQuery(getAuthorsQuery)
    const [addBook] = useMutation(addBookMutation);
    const [name, setName] = useState(null)
    const [genre, setGenre] = useState(null)
    const [authorId, setAuthorId] = useState(null)

    function displayAuthors(params) {
        if (loading) return <option>Loading Authors</option>
        else return data.authors.map(author => <option key={author.id} value={author.id}>{author.name}</option>)
    }

    function submitForm(e) {
        e.preventDefault()
        addBook({ variables: { name: name, genre: genre, authorId: authorId }, refetchQueries: [{ query: getBooksQuery }] })
        refetch()
    }

    return (
        <form id="add-book" onSubmit={submitForm}>
            <div className="field">
                <label>Book Name:</label>
                <input type="text" onChange={e => setName(e.target.value)} />
            </div>
            <div className="field">
                <label>Genre:</label>
                <input type="text" onChange={e => setGenre(e.target.value)} />
            </div>
            <div className="field">
                <label>Author:</label>
                <select onChange={e => setAuthorId(e.target.value)}>
                    <option>Select Author</option>
                    {displayAuthors()}
                </select>
            </div>
            <button>+</button>
        </form>
    )
}

export default AddBooks;
