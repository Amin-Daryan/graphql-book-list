import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import { getBooksQuery } from "../queries/queries"

// Components
import BookDetails from "./BookDetails"


function BookList() {
    const { loading, data } = useQuery(getBooksQuery)
    const [selectedBook, setSelectedBook] = useState(null)

    function displayBooks() {
        if (loading) return <div>Loading Books...</div>
        else return data.books.map(book => {
            return (<li key={book.id} onClick={() => setSelectedBook(book.id)}>{book.name}</li>)
        })
    }

    return (
        <div>
            <ul id="book-list">
                {displayBooks()}
            </ul>
            <BookDetails selectedBook={selectedBook}/>
        </div>
    )
}

export default BookList;
